* Install rsub in Sublime locally:

     * select `Tools > Command Palette`: type `Install Package` and select it
     * type `rsub` and select it (this will install it)
     * type `nano ~/.ssh/config` and add:
     ```
     Host your_remote_server 
         RemoteForward 52698 127.0.0.1:52698
     ```
* On your server/other machine:
    * `sudo wget -O /usr/local/bin/rsub https://raw.github.com/aurora/rmate/master/rmate`
    * `sudo chmod +x /usr/local/bin/rsub`
    * `rsub your_file.txt` (it will open in Sublime locally!)

Website: http://log.liminastudio.com/writing/tutorials/sublime-tunnel-of-love-how-to-edit-remote-files-with-sublime-text-via-an-ssh-tunnel
